#1. In a print statement, if I leave out left parenthes, interpreter will print out "SyntaxError: invalid syntax"
#if I leave out right parenthes, interpreter will print out "SyntaxError: unexpected EOF while parsing"
#if I leave out both of the parentheses, interpreter will print out "SyntaxError: invalid syntax"
#2. trying to print a string, if I leave one of the quotation marks, interpreter will print out "SyntaxError: EOL while scanning string literal"
#if I leave both of quotation marks, interpreter will print out "NameError: name 'Hello' is not defined"
#3. if I put a plus sign before a number, interpreter will print out the number without plus
# if I put "2++2", interpreter will print out "4"
#4. interpreter will print out "leading zeros in decimal integer literals are not permitted; use an 0o prefix for octal integers"
#5. If values are separated by probel, interpreter will print out "SyntaxError: invalid syntax". Else interpreter will print out new number, which consist on these two values
