import re

pattern = '^M?M?M?$'
print(re.search(pattern, 'M'))
print(re.search(pattern, 'MM'))
print(re.search(pattern, 'MMM'))
print(re.search(pattern, 'MMMM'))
print(re.search(pattern, ''))

pattern1 = '^M?M?M?(CM|CD|D?C?C?C?)$'
print(re.search(pattern1, 'MCM'))
print(re.search(pattern1, 'MD'))
print(re.search(pattern1, 'MMMCCC'))
print(re.search(pattern1, 'MCMC'))
print(re.search(pattern1, ''))
